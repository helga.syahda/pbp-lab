from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from lab_1.models import Friend
from .forms import FriendForm
from django.http import HttpResponseRedirect

# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    context = {}
    response = {}
    # create object of form
    form = FriendForm(request.POST or None)

    # check if form data is valid
    if form.is_valid():
        # save the form data to model
        form.save()
        if request.method == 'POST':
            return HttpResponseRedirect("/lab-3")
    else:
        print(form.errors)

    context['form'] = form
    return render(request, "lab3_form.html", response)