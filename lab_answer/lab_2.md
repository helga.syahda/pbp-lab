Nama    : Helga Syahda Elmira
NPM     : 2006463686
Kelas   : PBP A

1. Apakah perbedaan antara JSON dan XML? 
- JSON memiliki format berupa data interchange dan menggunakan JavaScript sedangkan XML memiliki format beruka markup language 
- JSON memperbolehkan penggunaan array sedangkan XML tidak memperbolehkan penggunaan array 
- JSON menggunakan struktur data map sedangkan XML menggunakan struktur data tree 
- JSON adalah format pertukaran data yang data-oriented sedangkan XML merupakan format pertukaran data yang Document-oriented 
- JSON tidak mendukung tipe data gambar sedangkan XML mendukung 
- JSON lebih tinggi tingkat keamanannya dibanding XML (kecuali saat menggunakan JSONP)
- Untuk transfer data JSON lebih cepat daripada XML 
- JSON lebih singkat sedangkan XML lebih besar dokumennya

JSON dapat digunakan pada layanan web yang didukung oleh protokol HTTP, contoh penggunaannya adalah untuk penyimpanan data di Database Mongodb. XML lebih dipakai/digunakan pada layanan web berbasis SOAP karena bisa dengan baik dibangun di atas XML.

JSON:
{"mahasiswa":[
    {"nama":"Helga","kelas":"A"}
]}

XML:
<mahasiswaUI>
 <mahasiswa>
   <nama>Helga</nama> <kelas>A</kelas>
 </mahasiswa>
</mahasiswaUI>

2. Apakah perbedaan antara HTML dan XML?
HTML mendeskripsikan struktur dari sebuah halaman web. HTML berisi beberapa macam elemen yang akan menyampaikan informasi kepada browser bagaimana untuk menampilkan konten.
Elemen HTML didefiniskan dengan tag pembuka (start tag), beberapa konten, dan tag penutup (end tag).

HTML berfungsi untuk menampilkan data dan ia terfokus pada bagaimana data tersebut ditampilkan (penampilan). Maka dari itu, HTML mendeskripsikan struktur dari web page. 
XML menstrukturkan, menyimpan, dan mentransfer informasi.

- HTML merupakan bahasa markup, sedangkan XML merupakan standard markup language yang mendefinisikan markup language yang lain
- HTML tidak case sensitive, sedangkan XML case sensitive
- HTML memiliki tag yang terbatas, sedangkan XML tagnya bisa dikembangkan
- HTML tidak menyediakan dukungan namespaces, sedangkan XML menyediakan dukungan namespaces
The namespace can be defined by an xmlns attribute in the start tag of an element.
"The namespace declaration has the following syntax. xmlns:prefix="URI"."
- HTML difokuskan pada penyajian data, sedangkan XML berfokus pada transfer data

Contoh XML ada diatas, contoh HTML ada di lab2.html :)


Referensi:
1. https://www.upgrad.com/blog/html-vs-xml/
2. https://blogs.masterweb.com/perbedaan-xml-dan-html/
3. https://www.niagahoster.co.id/blog/json-adalah/
4. Forum diskusi SCELE 
5. Pengertian di README.md gitlab
