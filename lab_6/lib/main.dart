import 'package:flutter/material.dart';


void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const appTitle = 'Informasi UDD';

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      title: appTitle,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.red,
        scaffoldBackgroundColor: Colors.red[100],
        
      ),
      scrollBehavior: const ConstantScrollBehavior(),
      home: MyHomePage(title: appTitle),
    );

  }
}

class ConstantScrollBehavior extends ScrollBehavior {
  const ConstantScrollBehavior();

  @override
  Widget buildScrollbar(BuildContext context, Widget child, ScrollableDetails details) => child;

  @override
  Widget buildOverscrollIndicator(BuildContext context, Widget child, ScrollableDetails details) => child;

  @override
  TargetPlatform getPlatform(BuildContext context) => TargetPlatform.macOS;

  @override
  ScrollPhysics getScrollPhysics(BuildContext context) => const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics());
}


class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
    
  Widget build(BuildContext context) {
    return Scaffold(
      body: 
      const MyCostumCard(),
      appBar: AppBar(title: Text(title)),
      

      drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.red,
              ),
              child: const Text('Donor Plasma',
              style: TextStyle(color: Colors.white, fontSize: 20.0))
            ),
            ListTile(
              title: const Text('Home'),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Daftar'),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Faq'),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Cari Pendonor'),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}



class SecondRoute extends StatelessWidget {
  const SecondRoute({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context){
    return Scaffold(
        appBar: AppBar(
          title: const Text("Menambahkan UDD"),
        ),
        body: const MyCostumForm()
    );
  }
}

class MyCostumForm extends StatefulWidget{
  const MyCostumForm ({Key? key}) : super(key: key);

  @override
  MyCostumFormState createState(){
    return MyCostumFormState();
  }
}

class MyCostumFormState extends State<MyCostumForm>{
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context){
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: TextFormField(
              decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                labelText: 'Nama UDD',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: TextFormField(
              decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                labelText: 'Golongan Darah yang Tersedia',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: TextFormField(
              decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                labelText: 'Jumlah Stok',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: TextFormField(
              decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                labelText: 'Nomor Telepon',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: TextFormField(
              decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                labelText: 'Alamat',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Center(
              child: ElevatedButton(
                child: const Text("Submit"),
                onPressed: (){
                  if (_formKey.currentState!.validate()){
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text("Berhasil menambahkan UDD")),
                    );
                    
                  }
                }
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class MyCostumCard extends StatefulWidget{
  const MyCostumCard({Key? key}) : super(key: key);

  @override
  MyCostumCardState createState(){
    return MyCostumCardState();
    
  }
}

class MyCostumCardState extends State<MyCostumCard>{

  @override
  Widget build(BuildContext context){
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          
          Container(
            margin: const EdgeInsets.all(15) ,
            alignment: Alignment.bottomCenter,
            decoration: const BoxDecoration(
            ),
            child: const Text(
              "Apabila anda tertarik untuk mendonorkan atau membutuhkan donor di UDD yang ada, mohon hubungi nomor telepon yang tertera untuk konfirmasi terlebih dahulu",
              style: TextStyle(color: Colors.grey, fontSize: 15.0),
            ),
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Card(
                // color: const Color.fromRGBO(0, 0, 70, 255),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    
                    const ListTile(
                      title: Text('\nUDD Semakin Sejahtera'),
                      subtitle: Text('\nAlamat: Bojonggede No. 200\nNomor Telepon: 0812101010\nGolongan Darah: A\nJumlah Stok: 9'),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        TextButton(
                          child: const Text('Edit'),
                          onPressed: () {/* ... */},
                        ),
                        const SizedBox(width: 8),
                        TextButton(
                          child: const Text('Delete'),
                          onPressed: () {/* ... */},
                        ),
                        const SizedBox(width: 8),
                      ],
                    ),
                  ]
                ),
              ),
            ),
          ),

           Center(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Card(
                // color: const Color.fromRGBO(0, 0, 70, 255),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    
                    const ListTile(
                      title: Text('\nUDD Jakarta Kota'),
                      subtitle: Text('\nAlamat: Jl. Gangnam No. 200, Daegu\nNomor Telepon: +82 6767543629\nGolongan Darah: B\nJumlah Stok: 7'),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        TextButton(
                          child: const Text('Edit'),
                          onPressed: () {/* ... */},
                        ),
                        const SizedBox(width: 8),
                        TextButton(
                          child: const Text('Delete'),
                          onPressed: () {/* ... */},
                        ),
                        const SizedBox(width: 8),
                      ],
                    ),
                  ]
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Center(
              child: ElevatedButton(
                  child: const Text("Menambahkan UDD"),
                  onPressed: (){
                    Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const SecondRoute()),
                    );
                  }
              ),
            ),
          ),
        ]
    );
  }
}

/**References
 * https://donorplasma.herokuapp.com/informasiUDD/
 * https://api.flutter.dev/flutter/material/ElevatedButton-class.html
 * https://flutter.dev/docs/cookbook/navigation/named-routes
 * https://api.flutter.dev/flutter/material/Card-class.html
 * https://flutter.dev/docs/cookbook/navigation/navigation-basics
 * https://flutter.dev/docs/cookbook/design/drawer
 * https://api.flutter.dev/flutter/widgets/Text-class.html
 * https://stackoverflow.com/questions/49369950/flutter-layout-container-margin
 * https://dartpad.dev/?id=9d0c605a96d5769df6dcd9bdd673320a&null_safety=true
 */
