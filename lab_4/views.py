from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm
from django.http import HttpResponseRedirect

# Create your views here.
def index(request):
    notes = Note.objects.all()  # TODO Implement this
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context = {}
    response = {}
    # create object of form
    form = NoteForm(request.POST or None)

    # check if form data is valid
    if form.is_valid():
        # save the form data to model
        form.save()
        if request.method == 'POST':
            return HttpResponseRedirect("/lab-4")
    else:
        print(form.errors)

    context['form'] = form
    return render(request, "lab4_form.html", response)

def note_list(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)